class SessionsController < ApplicationController
	def new
	end
       
        def log_in(user)
         session[:user_id] = user.id
        end

        def destroy
         log_out if logged_in?
         redirect_to root_url
        end
end
