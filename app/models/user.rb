# == Schema Information
#
# Table name: users
#
#  id                   :bigint           not null, primary key
#  address              :string
#  avatar_url           :string
#  birthday             :datetime
#  confirmation_sent_at :datetime
#  confirmation_token   :string
#  confirmed_at         :datetime
#  deleted_at           :datetime
#  email                :string           not null
#  fullname             :string
#  password_digest      :string           not null
#  role                 :integer          default(1), not null
#  created_at           :datetime
#  updated_at           :datetime
#
# Indexes
#
#  index_users_on_email  (email) UNIQUE
#
class User < ApplicationRecord
    has_secure_password

    
end
